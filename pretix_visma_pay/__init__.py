from django.utils.translation import gettext_lazy

try:
    from pretix.base.plugins import PluginConfig
except ImportError:
    raise RuntimeError("Please use pretix 2.7 or above to run this plugin!")

__version__ = '0.4.2'


class PluginApp(PluginConfig):
    name = 'pretix_visma_pay'
    verbose_name = 'Visma Pay'

    class PretixPluginMeta:
        name = 'Visma Pay'
        author = 'Jaakko Rinta-Filppula, Taneli Hongisto'
        description = gettext_lazy('Payment plugin for Visma Pay')
        visible = True
        version = __version__
        category = 'PAYMENT'
        compatibility = "pretix>=2.7.0"

    def ready(self):
        from . import signals  # NOQA


default_app_config = 'pretix_visma_pay.PluginApp'
